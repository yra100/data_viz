# Radiation data vizualization
We are analyzing radiation intensity ( aka amount of energy the radiation produces to the sensor ), and measured it for 4 full ress days ( and have a data for the whole year, but covered partially (training dataset for filtering the particles).
I tried to vizualize them as following

<!-- add some gif of the paper video: -->
<p align="center">
  <img src="Radiation/globe_north_pole.png" width = "420" height = "337"/>
<!-- </p> -->

But whatever plane I used, it seemed not continuous. So, I decided to add some animation

### Globe

<!-- add some gif of the paper video: -->
<p align="center">
  <img src="Radiation/globe.gif" width = "420" height = "337"/>
<!-- </p> -->

this looks much more interesting, but we see only one image, when in reality the radation intensity changes every day.

### Time dimensional data

<!-- add some gif of the paper video: -->
<p align="center">
  <img src="Radiation/globe_time.gif" width = "420" height = "337"/>
<!-- </p> -->

This looks much better and gives viewer an idea, how the radiation may change.

<!-- add some gif of the paper video: -->
<p align="center">
  <img src="Radiation/flat_time.gif" width = "420" height = "337"/>
<!-- </p> -->

